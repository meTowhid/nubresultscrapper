package com.stsbd.towhid.webview.scraper;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.stsbd.towhid.webview.R;

public class ScraperSample extends AppCompatActivity implements Scraper.CallBack {
    String
//            user = "04110200652", // me
//            user = "04130200773", // mahfuz
//            user = "04130100762", // shyla
            user = "04110200645", // enam
            pass = "123456";
    private MaterialDialog progressDialog;
    private Scraper scraper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog = new MaterialDialog.Builder(this)
                .title("Validating authentication...")
                .progressIndeterminateStyle(true)
                .content("Hold on tight!")
                .cancelable(false)
                .progress(true, 0)
                .autoDismiss(false)
                .positiveText("ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new MaterialDialog.Builder(ScraperSample.this).content(scraper.sb).show();
                    }
                }).show();

        scraper = new Scraper(this, user, pass, this);
        scraper.attachWebView((WebView) findViewById(R.id.web))
//                .skipLogin()
//                .verifyUser();
                .start();
//                .startFrom(Page.DASHBOARD);
    }

    @Override
    public void onLogin(Status status) {
        Toast.makeText(this, status.name(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProgress(Page page, String response) {
        progressDialog.setContent(response);
    }

    @Override
    public void onFinish(Status status) {
        progressDialog.dismiss();
        new MaterialDialog.Builder(this)
                .content(status.name() + "\n\n" + scraper.sb)
                .show();
    }

    @Override
    public void onError(Status status, String response) {
        Toast.makeText(this, status.name() + "\n" + response, Toast.LENGTH_LONG).show();
    }
}

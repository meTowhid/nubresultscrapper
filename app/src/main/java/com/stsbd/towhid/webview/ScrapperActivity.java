package com.stsbd.towhid.webview;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class ScrapperActivity extends AppCompatActivity {

    String user = "cse130200773", pass = "CSE130200773", currentURL;
//    String user = "cse130100762", pass = "762", currentURL;
    private WebView webView;
    private int currentPage = 1;
    private Context con;
    List<String> semesterKey = new ArrayList<>();
    private StringBuilder sb;
    private MaterialDialog pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sb = new StringBuilder();
        con = this;

         pb = new MaterialDialog.Builder(this)
//                .title("title")
                .title("Validating authentication...")
                 .progressIndeterminateStyle(true)
                 .progress(true, 0)
                .show();

        webView = (WebView) findViewById(R.id.web);
        webView.loadUrl("http://nubacad.com/");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(false);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(final WebView view, final String url) {
/*                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void start() {*/
                currentURL = url;
                if (currentPage == mCons.LOGIN) {
                    currentPage = mCons.HOME_TER;
                    view.loadUrl("javascript: {" +
                            "document.getElementById('username').value = '" + user + "';" +
                            "document.getElementById('password').value = '" + pass + "';" +
                            "document.getElementById('validation-form').submit();" +
                            " };");
                } else  if (!currentURL.contains("login")) // check if login successful
                    view.loadUrl("javascript:" +
                            "window.HTMLOUT.processHTML(" +
                            "'<head>'+document.getElementsByTagName('html')[0].innerHTML" +
                            "+'</head>');");
                else Toast.makeText(ScrapperActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
            }
               /* }, 2000);
            }*/
        });

    }

    /* An instance of this class will be registered as a JavaScript interface */
    class MyJavaScriptInterface {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html) {
            // process the html as needed by the app
            Document doc = Jsoup.parse(html);

            switch (currentPage) {
                case mCons.HOME_TER:
                    // grab current semesters taken coursed
                    sb.append("----------HOME----------\n")
                    .append("Current courses: \n");
                    try {
                        Elements courseResult = doc
                                .getElementsByClass("table table-striped table-bordered table-hover")
                                .first().getElementsByTag("tbody").first().getElementsByTag("tr");

                        for (int i = 0; i < courseResult.size() - 1; i++) {
                            Element e = courseResult.get(i);
                            String sub = e.getElementsByTag("td").text();
                            if (sub.length() > 10) sb.append(sub.substring(0, 10)).append("\n");
                        }
                    } catch (Exception e) {
                        sb.append("Empty!\n");
                    }
                    // after that..
                    // set current page and load new page in webView
                    currentPage = mCons.PROFILE;
                    break;
                case mCons.PROFILE:
                    // grab user profile information's here like name, mobile, email, picture
                    sb.append("\n----------PROFILE----------\n");
                    for (Element elem : doc.getElementsByClass("form-control input-transparent"))
                        sb.append(elem.attr("placeholder"))
                                .append(" - ")
                                .append(elem.val())
                                .append("\n");

                    sb.append("Profile pic - ")
                            .append(doc.getElementsByClass("widget large text-center").select("img").attr("src"))
                            .append("\n");


                    // after that..
                    // set current page and load new page in webView
                    currentPage = mCons.RESULT;
                    break;
                case mCons.RESULT:
                    sb.append("\n----------RESULT----------\n");
                    Element semesterInfo = doc.getElementById("semester");
                    for (Element elem : semesterInfo.getAllElements()) semesterKey.add(elem.val());
                    semesterKey.remove(0); // removes the first result spinner
                    currentPage = mCons.RESULT_SPINNER;
                    break;
                case mCons.RESULT_SPINNER:
                    // process spinner changes and grab data
                    sb.append("------------------\n");
                    try {
                        Elements courseResult = doc
                                .getElementsByClass("table table-striped table-bordered table-hover")
                                .first().getElementsByTag("tbody").first().getElementsByTag("tr");

                        for (int i = 0; i < courseResult.size() - 1; i++) {
                            Element e = courseResult.get(i);
                            String sub = e.getElementsByTag("td").get(0).text();
                            String grade = e.getElementsByTag("td").get(1).text();
                            if (sub.length() > 10)
                                sb.append(sub.substring(0, 10))
                                        .append(" - ")
                                        .append(grade)
                                        .append("\n");
                        }
                    } catch (Exception e) {
                        sb.append("TER is not filled up\n");
                    }
                    break;
            }

            ScrapperActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (currentPage) {
                        case mCons.PROFILE:
                            pb.setTitle("fetching from profile");
                            webView.loadUrl("http://182.160.97.204:8285/nubter/panel/profile");
                            break;
                        case mCons.RESULT:
                            pb.setTitle("fetching from result");
                            webView.loadUrl("http://182.160.97.204:8285/nubter/panel/result");
                            break;
                        case mCons.RESULT_SPINNER:
                            if (!semesterKey.isEmpty()) {
//                                Toast.makeText(ScrapperActivity.this, semesterKey.size() + " elements. trying to load\n" + semesterKey.get(0), Toast.LENGTH_SHORT).show();
                                pb.setTitle("Collecting semesters data");
                                webView.loadUrl("javascript: {" +
                                        "$('#semester').val('" + semesterKey.get(0) + "');" +
                                        "$('form').submit();" +
                                        " };");
                                semesterKey.remove(0);
                            } else {
                                pb.dismiss();
                                new MaterialDialog.Builder(con).content(sb).show();
                            }
                            break;
                    }
                }
            });
        }
    }

}

package com.stsbd.towhid.webview;

import android.app.Application;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

/**
 * Created by ehsan on 08-Mar-17.
 */

public class mCons extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public static final String BASE_URL = "http://182.160.97.196:8088/";
    public static final String LOGIN_PAGE = "studentportal/site/login";
    public static final String AUTH_PAGE = "studentportal/site/authenticate";
    public static final String RESULT_PAGE = "studentportal/acdstud/partialresult";

    public static final int ERROR = 0;
    public static final int LOGIN = 1;
    public static final int HOME_TER = 2;
    public static final int PROFILE = 3;
    public static final int RESULT = 4;
    public static final int RESULT_SPINNER = 5;

    public static final int DASHBOARD = 2;
}

package com.stsbd.towhid.webview.scraper;

/**
 * Created by towhid on 12-Aug-17.
 */

public enum Page {
    BASE("http://182.160.97.196:8088/", Status.WAITING),
    DASHBOARD(BASE.URL + "studentportal/", Status.WAITING),
    LOGIN(DASHBOARD.URL + "site/login", Status.WAITING),
    PROFILE(DASHBOARD.URL + "acdstud/view", Status.WAITING), // user profile information
    AUTHENTICATE(DASHBOARD.URL + "site/authenticate", Status.WAITING),
    RESULT(DASHBOARD.URL + "acdstud/partialresult", Status.WAITING), // result by semesters
    COURSES_ROUTINE(DASHBOARD.URL + "acdstud/semreg", Status.WAITING), // current courses and routine
    CREDIT_CGPA(DASHBOARD.URL + "acdstud/partialresultnosemester", Status.WAITING); // total credit and cgpa

    public String URL;
    public Status status;

    Page(String URL, Status status) {
        this.URL = URL;
        this.status = status;
    }
}

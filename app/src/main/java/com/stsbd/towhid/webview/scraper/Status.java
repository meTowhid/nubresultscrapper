package com.stsbd.towhid.webview.scraper;

/**
 * Created by towhid on 6/18/17.
 */

public enum Status {
    WAITING,
    ERROR,
    LOGIN_FAILED,
    LOGIN_SUCCESS,
    FINISHED
}

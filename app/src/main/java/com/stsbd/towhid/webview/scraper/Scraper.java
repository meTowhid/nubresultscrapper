package com.stsbd.towhid.webview.scraper;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.stsbd.towhid.webview.scraper.Page.COURSES_ROUTINE;
import static com.stsbd.towhid.webview.scraper.Page.CREDIT_CGPA;
import static com.stsbd.towhid.webview.scraper.Page.DASHBOARD;
import static com.stsbd.towhid.webview.scraper.Page.LOGIN;
import static com.stsbd.towhid.webview.scraper.Page.RESULT;
import static com.stsbd.towhid.webview.scraper.Status.FINISHED;
import static com.stsbd.towhid.webview.scraper.Status.LOGIN_FAILED;
import static com.stsbd.towhid.webview.scraper.Status.LOGIN_SUCCESS;

/**
 * Created by towhid on 16-Jun-17.
 */

public class Scraper {
    private final Handler handler;
    private final Activity activity;
    private JSONObject data, currentSummary;
    private JSONArray resultsList;
    public StringBuilder sb;
    private Page page = LOGIN;
    private boolean singlePage, skipLogin, isLoginCheckNotified;
    private CallBack callBack;
    private List<Double> resList;
    private String id, pass;
    private WebView webView;

    public Scraper(Activity activity, String id, String pass, CallBack callBack) {
        this.id = id;
        this.pass = pass;
        this.activity = activity;
        this.callBack = callBack;
        sb = new StringBuilder();
        handler = new Handler();
        data = new JSONObject();
        currentSummary = new JSONObject();
        resultsList = new JSONArray();
    }

    Scraper attachWebView(WebView webView) {
        this.webView = webView;
        return this;
    }

    Scraper skipLogin() {
        this.skipLogin = true;
        return this;
    }

    void verifyUser() {
        singlePage = true;
        start();
    }

    private void serverResponseCheck(final Page p) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (page == p) {
                    webView.stopLoading();
                    callBack.onFinish(Status.ERROR);
                    callBack.onError(Status.ERROR, "Server is down for maintenance.");
                }

            }
        }, 15000);
    }

    void start() {
        if (skipLogin) startFrom(DASHBOARD);
        else {
            initWebView();
            serverResponseCheck(page);
            callBack.onProgress(page, "Status LOGIN..");
            webView.loadUrl(LOGIN.URL);
        }
    }

    void startFrom(Page p) {
        page = p == RESULT ? DASHBOARD : p;
        this.isLoginCheckNotified = true;
        serverResponseCheck(page);
        this.skipLogin = true;
        initWebView();

        switch (page) {
            case DASHBOARD:
                webView.loadUrl(DASHBOARD.URL);
                break;
            case CREDIT_CGPA:
                webView.loadUrl(CREDIT_CGPA.URL);
                break;
            case COURSES_ROUTINE:
                webView.loadUrl(COURSES_ROUTINE.URL);
                break;
            default:
                page.status = Status.ERROR;
                callBack.onError(page.status, "Wrong page");
                callBack.onFinish(page.status);
                return;
        }
        callBack.onProgress(p, "Status " + page.name());
    }

    private void initWebView() {
        if (!skipLogin) CookieManager.getInstance().setAcceptCookie(false);
        if (webView == null) webView = new WebView(activity);

        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
            webView.getSettings().setSavePassword(false);
        webView.getSettings().setLoadsImagesAutomatically(false);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(final WebView view, final String url) {
                if (page == LOGIN) {
                    page = DASHBOARD;
                    if (!skipLogin) CookieManager.getInstance().setAcceptCookie(true);
                    callBack.onProgress(page, "attempting to login...");
                    view.loadUrl("javascript: {" +
                            "document.getElementById('input-username').value = '" + id + "';" +
                            "document.getElementById('input-password').value = '" + pass + "';" +
                            "document.forms[0].submit();" +
                            " };");
                } else if (url.equals(LOGIN.URL)) {
                    // Authentication failed - maybe wrong password
                    page.status = LOGIN_FAILED;
                    callBack.onLogin(page.status);
                    callBack.onError(page.status, "Invalid userId or password.");
                    callBack.onFinish(page.status);
                } else if (singlePage) {
                    page.status = LOGIN_SUCCESS;
                    callBack.onLogin(page.status);
                    callBack.onFinish(page.status);
                } else {
                    if (!isLoginCheckNotified) {
                        isLoginCheckNotified = true;
                        callBack.onLogin(LOGIN_SUCCESS);
                    }
                    callBack.onProgress(page, "Starting to fetch data from... " + page);
                    view.loadUrl("javascript:" +
                            "window.HTMLOUT.processHTML(" +
                            "'<head>'+document.getElementsByTagName('html')[0].innerHTML" +
                            "+'</head>');");
                }
            }
        });
    }

    public interface CallBack {
        void onLogin(Status status);

        void onProgress(Page page, String response);

        void onFinish(Status status);

        void onError(Status status, String response);
    }

    /* An instance of this class will be registered as a JavaScript interface */
    private class MyJavaScriptInterface {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html) {
            Document doc = Jsoup.parse(html);

            switch (page) { // fetching mechanism
                case DASHBOARD:
                    sb.append("----------DASHBOARD----------\n");

                    String name = doc.getElementsByClass("username").text();
                    sb.append(name).append("\n");
                    try {
                        currentSummary.put("name", name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Element script = doc.getElementsByClass("site-index").first(); // Get the script part
                    Pattern p = Pattern.compile("\\[\\[(.*?)\\]\\]"); // Regex for the value of the key
                    Matcher m = p.matcher(script.html()); // you have to use html here and NOT text! Text will drop the 'key' part
                    m.find();

                    try {
                        resList = new ArrayList<>();
                        JSONArray resultArray = new JSONArray(m.group());
                        //Logger.json(resultArray.toString());
                        for (int j = 0; j < resultArray.length(); j++) {
                            String title = resultArray.getJSONArray(j).get(0).toString();
                            double gp = Double.parseDouble(resultArray.getJSONArray(j).get(1).toString());
                            //sb.append(title).append(" - ").append(gp).append("\n");
//                            Map<String, Double> map = new HashMap<>();
//                            map.put(title, gp);
                            resList.add(gp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case PROFILE:
                    sb.append("----------PROFILE----------\n");


                    break;
                case RESULT:
                    // Parsing whole result records
                    sb.append("----------RESULT----------\n");
                    try {
                        Elements semesterElements = doc
                                .getElementsByClass("span3").first()
                                .getElementsByClass("table table-striped table-bordered");
                        Elements semesterTitles = doc
                                .getElementsByClass("span3").first()
                                .getElementsByClass("alert alert-info");

                        JSONArray resultBoard = new JSONArray();
                        int completedCredits = 0;
                        double cgpa = 0;

                        int totalCredit, courseCounter, semesterCounter = 0;
                        for (Element semester : semesterElements) {
                            JSONObject semesterObject = new JSONObject();
                            JSONObject subObject = new JSONObject();
                            JSONArray subArray = new JSONArray();
                            courseCounter = 1;
                            totalCredit = 0;


                            for (Element course : semester.getElementsByTag("tr")) {
                                Elements item = course.getElementsByTag("td"); // get table data
                                if (item.isEmpty()) continue; // if it's not table data then skip

                                JSONObject sub = new JSONObject();
                                /*sub.put("code", item.get(0).text());
                                sub.put("title", item.get(1).text());
                                sub.put("credit", item.get(2).text());
                                sub.put("grade_letter", item.get(3).text());
                                sub.put("grade_point", item.get(4).text());*/
//                                sub.put(item.get(0).text(), Double.parseDouble(item.get(4).text()));
                                subObject.put(item.get(0).text(), Double.parseDouble(item.get(4).text()));
                                totalCredit += Integer.parseInt(item.get(2).text().charAt(0) + "");
                                courseCounter++;
                            }


                            String[] title_gpa = semesterTitles.get(semesterCounter).text().split("CGPA- ");

                            semesterCounter++;
//                            semesterObject.put("semester_title", title_gpa[0]);
//                            semesterObject.put("semester_number", semesterCounter);
                            semesterObject.put("total_credits", totalCredit);
                            semesterObject.put("gpa", Double.parseDouble(title_gpa[1]));
                            semesterObject.put("subjects_list", subObject);
                            resultBoard.put(semesterObject);


                            completedCredits += totalCredit;
                            cgpa += (Double.parseDouble(title_gpa[1]) * totalCredit);
                        }

                        cgpa /= completedCredits;
                        currentSummary.put("cgpa", cgpa);
                        currentSummary.put("totalCredit", completedCredits);
                        currentSummary.put("completedSemester", semesterCounter);

                        data.put("results", resultBoard);
                        data.put("currentSummary", currentSummary);

                        Logger.json(data.toString());
                    } catch (Exception e) {
                        Logger.d(e);
                    }
                    break;
                case CREDIT_CGPA:
                    sb.append("----------CREDIT_CGPA----------\n");
                    // todo
                    break;
                case COURSES_ROUTINE:
                    sb.append("----------COURSES_ROUTINE----------\n");
                    // todo
                    break;
            }

            // this controls the whole scraping process
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (page) {
                        case DASHBOARD:
                            page = RESULT; // setup next page
                            callBack.onProgress(page, "Status RESULT"); // notify activity
                            webView.loadUrl(RESULT.URL); // load in webview
                            break;
                        case RESULT:
                            page = CREDIT_CGPA;
                            callBack.onProgress(page, "Status PARTIAL_TRANSCRIPT");
                            webView.loadUrl(CREDIT_CGPA.URL);
                            break;
                        case CREDIT_CGPA:
                            page = COURSES_ROUTINE;
                            callBack.onProgress(page, "Status COURSES_ROUTINE_PAGE");
                            webView.loadUrl(COURSES_ROUTINE.URL);
                            break;
                        default: //case COURSES_ROUTINE:
                            page.status = FINISHED;
                            callBack.onProgress(page, sb.toString());
                            callBack.onFinish(page.status);
                    }
                }
            });
        }
    }
}

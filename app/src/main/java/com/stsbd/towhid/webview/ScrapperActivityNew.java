package com.stsbd.towhid.webview;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.afollestad.materialdialogs.MaterialDialog;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScrapperActivityNew extends AppCompatActivity {


    public static final String
            BASE_URL = "http://182.160.97.196:8088/",
            LOGIN_PAGE = "studentportal/site/login",
            PROFILE_PAGE = "studentportal/acdstud/view", // user profile information
            AUTH_PAGE = "studentportal/site/authenticate",
            RESULT_PAGE = "studentportal/acdstud/partialresult", // result by semesters
            SEMESTER_REGESTRATION_PAGE = "studentportal/acdstud/semreg", // current courses and routine
            PARTIAL_TRANSCRIPT_PAGE = "studentportal/acdstud/partialresultnosemester"; // total credit and cgpa

    public static final int
            ERROR = 0,
            LOGIN = 1,
            DASHBOARD = 2,
            PROFILE = 3,
            RESULT = 4,
            PARTIAL_TRANSCRIPT = 5,
            SEMESTER_REGESTRATION = 6;

    String currentURL = BASE_URL + LOGIN_PAGE,
    //            user = "04110200652", // me
    user = "04130200773", // mahfuz
            pass = "123456";
    private List<Double> resList;
    //    private List<Map<String, Double>> resList;
    private MaterialDialog progressDialog;
    private int nextPage = LOGIN;
    private StringBuilder sb;
    private WebView webView;
    private Activity con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sb = new StringBuilder();
        con = this;

        progressDialog = new MaterialDialog.Builder(this)
                .title("Validating authentication...")
                .progressIndeterminateStyle(true)
                .content("Hold on tight!")
                .cancelable(false)
                .progress(true, 0)
                .show();

        CookieManager.getInstance().setAcceptCookie(false);
//        webView = new WebView(this);
        webView = (WebView) findViewById(R.id.web);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(false);
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(final WebView view, final String url) {
                switch (nextPage) {
                    case LOGIN:
                        nextPage = DASHBOARD;
                        CookieManager.getInstance().setAcceptCookie(true);
                        progressDialog.setTitle("Status dashboard...");
                        view.loadUrl("javascript: {" +
                                "document.getElementById('input-username').value = '" + user + "';" +
                                "document.getElementById('input-password').value = '" + pass + "';" +
                                "document.forms[0].submit();" +
                                " };");
                        break;
                    default:
                        if (url.equals(BASE_URL + LOGIN_PAGE)) {
                            // Authentication failed - maybe wrong password
                            new MaterialDialog.Builder(con).content("Login failed!").show();
                            progressDialog.dismiss();
                            return;
                        }
                        view.loadUrl("javascript:" +
                                "window.HTMLOUT.processHTML(" +
                                "'<head>'+document.getElementsByTagName('html')[0].innerHTML" +
                                "+'</head>');");
                }
            }
        });
        webView.loadUrl(currentURL);

    }

    /* An instance of this class will be registered as a JavaScript interface */
    class MyJavaScriptInterface {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html) {
            Document doc = Jsoup.parse(html);
            //Logger.d(doc);

            switch (nextPage) { // fetching mechanism
                case DASHBOARD:
                    sb.append("----------DASHBOARD----------\n");

                    String name = doc.getElementsByClass("username").text();
                    sb.append(name).append("\n");


                    Element script = doc.getElementsByClass("site-index").first(); // Get the script part

                    Pattern p = Pattern.compile("\\[\\[(.*?)\\]\\]"); // Regex for the value of the key
                    Matcher m = p.matcher(script.html()); // you have to use html here and NOT text! Text will drop the 'key' part
                    m.find();

                    try {
                        resList = new ArrayList<>();
                        JSONArray resultArray = new JSONArray(m.group());
                        for (int j = 0; j < resultArray.length(); j++) {
                            String title = resultArray.getJSONArray(j).get(0).toString();
                            double gp = (double) resultArray.getJSONArray(j).get(1);
                            //sb.append(title).append(" - ").append(gp).append("\n");
//                            Map<String, Double> map = new HashMap<>();
//                            map.put(title, gp);
                            resList.add(gp);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Logger.d(sb);
                    break;
                /*case PROFILE:
                    sb.append("----------PROFILE----------\n");


                    break;*/
                case RESULT:
                    // Parsing whole result records
                    sb.append("----------RESULT----------\n");
                    try {
                        Elements semesterElements = doc
                                .getElementsByClass("span3").first()
                                .getElementsByClass("table table-striped table-bordered");
                        Elements semesterTitles = doc
                                .getElementsByClass("span3").first()
                                .getElementsByClass("alert alert-info");

                        int j, i = 0;
                        for (Element semester : semesterElements) {
                            j = 1;
                            String semesterTime = (i + 1) + " - " + semesterTitles.get(i).text();
                            sb.append(semesterTime).append(" --- ").append(resList.get(i)).append("\n"); // Summer 2011
                            for (Element course : semester.getElementsByTag("tr")) {
                                Elements item = course.getElementsByTag("td");
                                if (item.isEmpty()) continue;
                                String subject = /*(i + 1) +*/ "  -> " + j + ". " // 1_1
                                        + item.get(0).text() // Course code
//                                        + " - " + item.get(1).text() // Course name
//                                        + "\nGrade: "
//                                        + item.get(4).text() // Grade point
                                        + "___"
                                        + item.get(3).text() // Grade letter
//                                        + "\nCredit: "
                                        + "___"
                                        + item.get(2).text(); // Credit

                                sb.append(subject).append("\n");
                                j++;
                            }
                            i++;
                        }
                    } catch (Exception e) {
                        Logger.d(e);
                    }
                    break;
            }

            con.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (nextPage) {
                        case DASHBOARD:
                            nextPage = PROFILE;
                            progressDialog.setTitle("Fetching Profile information");
                            webView.loadUrl(BASE_URL + PROFILE_PAGE);
                            break;
                        case PROFILE:
                            nextPage = RESULT;
                            progressDialog.setTitle("Fetching Results");
                            webView.loadUrl(BASE_URL + RESULT_PAGE);
                            break;
                        default:
                            progressDialog.dismiss();
                            new MaterialDialog.Builder(con).content(sb).show();
                    }
                }
            });
        }
    }

}
